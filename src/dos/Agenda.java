package dos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
public class Agenda {

	public static void main(String[] args) throws IOException {
		
		Map<String,Integer> contactos = new TreeMap<>();
		BufferedReader linea = new BufferedReader(new InputStreamReader(System.in));
		boolean fin = false;
		
		
		do {
			System.out.print("> ");
			
			Scanner sc = new Scanner(linea.readLine());
			sc.useDelimiter(":");
			String s = sc.next();
			
			
			if(s.equalsIgnoreCase("buscar")) {
				String nombre =sc.next();
				Integer tlfn = contactos.get(nombre);
				if(tlfn !=null) {
					System.out.println("telefono:"+tlfn);
				}else {
					System.out.println("telefono no existe");

				}
	
			}else if(s.equalsIgnoreCase("eliminar")){
				String nombre =sc.next();
				Integer tlfn = contactos.remove(nombre);
				if(tlfn ==null) {
					System.out.println("telefono no encontrado");
				}
				
			}else if(s.equalsIgnoreCase("salir")){
				fin =true;
			}else {
				try {	
					 int tlfn =sc.nextInt();
					 Integer anterior= contactos.put(s, tlfn);
					if(anterior != null) {
						System.out.printf("el telefono %d se ha cambiado por el telefono");
					}
				}catch (InputMismatchException e) {
					System.out.println("No has introducido un telefono");
				}
			
				
			}
		}while(!fin);

	}

}
