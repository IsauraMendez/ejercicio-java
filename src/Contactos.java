
public class Contactos {
	String nombre;
	String telfn;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelfn() {
		return telfn;
	}
	public void setTelfn(String telfn) {
		this.telfn = telfn;
	}
	
	public Contactos(String nombre, String telfn) {
		super();
		this.nombre = nombre;
		this.telfn = telfn;
	}
	public Contactos() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Contactos [nombre=" + nombre + ", telfn=" + telfn + "]";
	}
	

}
